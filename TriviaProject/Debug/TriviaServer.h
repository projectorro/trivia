#pragma once
#include "sqlite3.h"
#include "RecievedMessage.h"
#include "Room.h"
#include "DataBase.h"
#include "Validator.h"
#include "Helper.h"
#include <iostream>
#include <map>
#include <thread>
#include <string>
#include <queue>
#include <mutex>
#include <winsock2.h>

#define PORT 8820

using namespace std;

class TriviaServer
{
public:
	TriviaServer();
	~TriviaServer();

	void server();
	
private:
	void bindAndListen();
	void accept();
	void clientHandler(SOCKET);
	void safeDeleteUser(RecievedMessage*);

	User* handleSignin(RecievedMessage*);
	bool handleSignup(RecievedMessage*);
	void handleSignout(RecievedMessage*);

	void handleLeaveGame(RecievedMessage*);
	void handleStartGame(RecievedMessage*);
	void handlePlayerAnswer(RecievedMessage*);

	bool handleCreateRoom(RecievedMessage*);
	bool handleCloseRoom(RecievedMessage*);
	bool handleJoinRoom(RecievedMessage*);
	bool handleLeaveRoom(RecievedMessage*);
	void handleGetUsersInRoom(RecievedMessage*);
	void handleGetRooms(RecievedMessage*);

	void handleGetBestScores(RecievedMessage*);
	void handleGetPersonalStatus(RecievedMessage*);
	
	void handleRecievedMessages();
	void addRecievedMessage(RecievedMessage*);
	RecievedMessage* buildRecieveMessage(SOCKET, int);
	
	User* getUserByName(string);
	User* getUserBySocket(SOCKET);
	Room* getRoomById(int);
	
	map<string, string> users;
	SOCKET _serverSocket;
	map<SOCKET, User*> _connectedUsers;
	DataBase _db;
	map<int, Room*> _roomList;
	mutex _mtxRecievedMessages;
	queue<RecievedMessage*> _queRcvMessages;
	static int _roomIdSequence;
};

