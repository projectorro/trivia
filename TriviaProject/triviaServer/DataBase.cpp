#include "DataBase.h"

char *zErrMsg = 0;
bool isUserExist = false;

int countUsers = 0;
vector<Question*> questions;
vector<string> status;
int gameId = 0;

map<string, int> usersAndScores;

DataBase::DataBase()
{
	ifstream f("database.db");
	if (f.good())//exists
	{
		f.close();
		remove("database.db");
	}
	else f.close();
	int rc = sqlite3_open("database.db", &_db);
	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(_db) << endl;
		sqlite3_close(_db);
		throw exception();
	}
	rc = sqlite3_exec(_db, "create table t_users(username text primary key not null, password text not null, email text not null);", NULL, NULL, &zErrMsg);
	if (rc)
	{
		cout << "Can't create table t_users: " << sqlite3_errmsg(_db) << endl;
		sqlite3_close(_db);
		throw exception();
	}
	rc = sqlite3_exec(_db, "create table t_questions(question_id integer primary key autoincrement, question text not null, correct_ans text not null, ans2 text not null, ans3 text not null, ans4 text not null);", NULL, NULL, &zErrMsg);
	if (rc)
	{
		cout << "Can't open table t_questions: " << sqlite3_errmsg(_db) << endl;
		sqlite3_close(_db);
		throw exception();
	}
	//puts questions
	else {
		int rc = sqlite3_exec(_db, "insert into t_questions(question, correct_ans, ans2, ans3, ans4) values('Former Australian Prime Minister Kevin Rudd is proficient in which foreign language?', 'Chinese Mandarin', 'Turkish', 'Russian', 'Lao');", NULL, NULL, &zErrMsg);
		if(rc){ cout << sqlite3_errmsg(_db) << endl; }
		sqlite3_exec(_db, "insert into t_questions(question, correct_ans, ans2, ans3, ans4) values('In the fairytale, what did the Ugly Duckling turn into?', 'Swan', 'Frog', 'Prince', 'Witch');", NULL, NULL, &zErrMsg);
		sqlite3_exec(_db, "insert into t_questions(question, correct_ans, ans2, ans3, ans4) values('According to the WHO, the people of WHICH of these countries consume the most alcohol per head?', 'Ukraine', 'Uruguay', 'USA', 'UK');", NULL, NULL, &zErrMsg);
		sqlite3_exec(_db, "insert into t_questions(question, correct_ans, ans2, ans3, ans4) values('The capital city of WHICH country begins with the letter K?', 'Afghanistan', 'Argentina', 'Algeria', 'Australia');", NULL, NULL, &zErrMsg);
		sqlite3_exec(_db, "insert into t_questions(question, correct_ans, ans2, ans3, ans4) values('Bad luck is sometimes called Hard WHAT?', 'Lines', 'Marks', 'Spots', 'Squares');", NULL, NULL, &zErrMsg);
		sqlite3_exec(_db, "insert into t_questions(question, correct_ans, ans2, ans3, ans4) values('A person who never laughs is known as a WHAT?', 'Agelast', 'Gigglenaught', 'Nojoy', 'Gelotologer');", NULL, NULL, &zErrMsg);
		sqlite3_exec(_db, "insert into t_questions(question, correct_ans, ans2, ans3, ans4) values('In WHICH decade did Rod Steiger win a Best Actor Oscar for his role in In The Heat Of The Night?', '1960s', '1940s', '2000s', '1980s');", NULL, NULL, &zErrMsg);
		sqlite3_exec(_db, "insert into t_questions(question, correct_ans, ans2, ans3, ans4) values('WHAT is the national anthem of the nation of Tuvalu?', 'God Save The Queen', 'Polynesia The Gracious', 'Almighty Tuvalu', 'Keep The Sea Away');", NULL, NULL, &zErrMsg);
		sqlite3_exec(_db, "insert into t_questions(question, correct_ans, ans2, ans3, ans4) values('Which is the meaning of the word amity?', 'Friendship', 'Value', 'Horror', 'Scorn');", NULL, NULL, &zErrMsg);
		sqlite3_exec(_db, "insert into t_questions(question, correct_ans, ans2, ans3, ans4) values('Which of these words is closest in meaning to wither?', 'Wilt', 'Climate', 'If', 'Complain');", NULL, NULL, &zErrMsg);
		sqlite3_exec(_db, "insert into t_questions(question, correct_ans, ans2, ans3, ans4) values('Frank Burns was a character in WHICH TV series?', 'M*A*S*H', 'Moonlighting', 'Peep Show', 'Shameless');", NULL, NULL, &zErrMsg);
		sqlite3_exec(_db, "insert into t_questions(question, correct_ans, ans2, ans3, ans4) values('Which of the following is NOT a piece of mountain climbing equipment?', 'Hackle', 'Carabiner', 'Crampon', 'Piton');", NULL, NULL, &zErrMsg);
		sqlite3_exec(_db, "insert into t_questions(question, correct_ans, ans2, ans3, ans4) values('Who wrote the novel Light A Penny Candle?', 'Maeve Binchy', 'Jeffrey Archer', 'Colleen McCullough', 'John Grisham');", NULL, NULL, &zErrMsg);
		sqlite3_exec(_db, "insert into t_questions(question, correct_ans, ans2, ans3, ans4) values('In which sport do teams compete for the Americas Cup ? ', 'Yacht racing', 'Baseball', 'Tennis', 'Golf');", NULL, NULL, &zErrMsg);
		sqlite3_exec(_db, "insert into t_questions(question, correct_ans, ans2, ans3, ans4) values('The Bazooka anti-tank weapon was named after its resemblance to a what?', 'Musical Instrument', 'Twig broom', 'Asian fruit', 'Deadly plant');", NULL, NULL, &zErrMsg);
		sqlite3_exec(_db, "insert into t_questions(question, correct_ans, ans2, ans3, ans4) values('Nazi Rudolph Hess was one of the last prisoners to be held WHERE?', 'Tower Of London', 'Abu Ghraib', 'The Bastille', 'Alcatraz');", NULL, NULL, &zErrMsg);
		sqlite3_exec(_db, "insert into t_questions(question, correct_ans, ans2, ans3, ans4) values('The European Economic Community was established late in WHICH decade?', '1950s', '1970s', '1990s', '1930s');", NULL, NULL, &zErrMsg);
		sqlite3_exec(_db, "insert into t_questions(question, correct_ans, ans2, ans3, ans4) values('Which actor upset Americans by calling the USA a dumb puppy?', 'Johnny Depp', 'Mel Gibson', 'Jim Carrey', 'Clint Eastwood');", NULL, NULL, &zErrMsg);
		sqlite3_exec(_db, "insert into t_questions(question, correct_ans, ans2, ans3, ans4) values('Complete the phrase: As the actress said to the WHAT?', 'Bishop', 'Director', 'Politician', 'Actor');", NULL, NULL, &zErrMsg);
		sqlite3_exec(_db, "insert into t_questions(question, correct_ans, ans2, ans3, ans4) values('How many walking legs does a lobster have?', '10', '2', '4', '6');", NULL, NULL, &zErrMsg);
		sqlite3_exec(_db, "insert into t_questions(question, correct_ans, ans2, ans3, ans4) values('Which is another word for a penitentiary?', 'Jail', 'Restaurant', 'Hospital', 'University');", NULL, NULL, &zErrMsg);
	}
	rc = sqlite3_exec(_db, "create table t_games(game_id INTEGER primary key AUTOINCREMENT not null, status INTEGER not null, start_time date not null, end_time date);", NULL, NULL, &zErrMsg);
	if (rc)
	{
		cout << "Can't open table t_games: " << sqlite3_errmsg(_db) << endl;
		sqlite3_close(_db);
		throw exception();
	}
	rc = sqlite3_exec(_db, "create table t_players_answers(game_id INTEGER not null, username text not null, question_id INTEGER not null, player_answer text not null, is_correct INTEGER not null, answer_time INTEGER not null, foreign key(game_id) REFERENCES t_games(game_id), foreign key(username) REFERENCES t_users(username), foreign key(question_id) REFERENCES t_questions(question_id));", NULL, NULL, &zErrMsg);
	if (rc)
	{
		cout << "Can't open table t_players_answers: " << sqlite3_errmsg(_db) << endl;
		sqlite3_close(_db);
		throw exception();
	}
}

DataBase::~DataBase()
{
	sqlite3_close(_db);
}

bool DataBase::isUserExists(string username)
{
	countUsers = 0;
	stringstream command;
	command << "select * from t_users where username == " << username << ";";
	sqlite3_exec(_db, command.str().c_str(), callbackCount, NULL, &zErrMsg);
	if (countUsers == 0) return false;
	else if (countUsers == 1) return true;
	else { cout << "Error from isUserExists: couple of users with the same uname."; return false; }
}

bool DataBase::addNewUser(string username, string password, string email)
{
	stringstream command;
	command << "insert into t_users(username, password, email) values('" << username << "', '" << password << "', '" << email << "');";
	int rc = sqlite3_exec(_db, command.str().c_str(), NULL, NULL, &zErrMsg);
	if (rc)
	{
		cout << "Can't insert the new user to database: " << sqlite3_errmsg(_db) << endl;
		return false;
	}
	return true;
}

bool DataBase::isUserAndPassMatch(string username, string password)
{
	countUsers = 0;
	stringstream command;
	command << "select * from t_users where username == " << username << " and password == " << password << ";";
	sqlite3_exec(_db, command.str().c_str(), callbackCount, NULL, &zErrMsg);
	if (countUsers == 0) return false;
	else if (countUsers == 1) return true;
	else { cout << "Error from isUserAndPassMatch: couple of users with the same uname and pword."; return false; }
}

vector<Question*> DataBase::initQuestions(int questionsNo)
{
	questions.clear();
	stringstream command;
	command << "select * from t_questions order by random() limit " << to_string(questionsNo) << ";";
	sqlite3_exec(_db, command.str().c_str(), callbackQuestions, NULL, &zErrMsg);
	return questions;
}

vector<string> DataBase::getBestScores()
{
	usersAndScores.clear();
	//"[uname1_len][uname1][score][uname2_len][uname2][score][uname3_len][uname3][score]"
	vector<string> bestScores;
	stringstream command;
	command << "select * from t_players_answers;";
	sqlite3_exec(_db, command.str().c_str(), callbackBestScores, NULL, &zErrMsg);
	string username1 = "", username2 = "", username3 = ""; //best scores username
	int score1 = 0, score2 = 0, score3 = 0, curr_score = 0;
	//checks which users has the highest scores
	{
		for (map<string, int>::iterator it = usersAndScores.begin(); it != usersAndScores.end(); it++)
		{
			curr_score = it->second;
			if (curr_score > score1)
			{
				score1 = curr_score;
				username1 = it->first;
			}
			else if (curr_score > score2)
			{
				score2 = curr_score;
				username2 = it->first;
			}
			else if (curr_score > score3)
			{
				score3 = curr_score;
				username3 = it->first;
			}
		}
	}

	//fixing scores string
	string score1_str = Helper().getPaddedNumber(score1, 7 - to_string(score1).length());
	string score2_str = Helper().getPaddedNumber(score2, 7 - to_string(score2).length());
	string score3_str = Helper().getPaddedNumber(score3, 7 - to_string(score3).length());

	//pushes all three users to vector
	{
		stringstream length1;
		if (username1.length() == 0) length1 << "00";
		else if (username1.length() < 10)
			length1 << "0";
		length1 << username1.length();
		bestScores.push_back(length1.str());
		bestScores.push_back(username1);
		bestScores.push_back(score1_str);

		stringstream length2;
		if (username2.length() == 0) length2 << "00";
		else if (username2.length() < 10)
			length2 << "0";
		length2 << username2.length();
		bestScores.push_back(length2.str());
		bestScores.push_back(username2);
		bestScores.push_back(score2_str);

		stringstream length3;
		if (username3.length() == 0) length3 << "00";
		else if (username3.length() < 10)
			length3 << "0";
		length3 << username3.length();
		bestScores.push_back(length3.str());
		bestScores.push_back(username3);
		bestScores.push_back(score3_str);
	}

	return bestScores;
}

int numOfGames = 0;
int correctAns = 0;
int questionsNum = 0;
float timeForAllQuestions = 0;

vector<string> DataBase::getPersonalStatus(string username)
{
	//status contains: number of games (4 bytes), number of right answers (6 bytes),
	//number of wrong answers (6 bytes) and avarage answer time (4 bytes for example: 0354 is 3.54)
	numOfGames = 0;
	correctAns = 0;
	questionsNum = 0;
	timeForAllQuestions = 0;
	stringstream command;
	command << "select * from t_players_answers where username == '" << username << "';";
	int rc = sqlite3_exec(_db, command.str().c_str(), callbackPersonalStatus, NULL, &zErrMsg);
	if (rc)
	{
		cout << "Something is wrong with getpersonalstatus: " << sqlite3_errmsg(_db) << endl;
		throw exception();
	}
	int numOfWrongAnswers = questionsNum - correctAns;
	float averageTime = timeForAllQuestions / questionsNum;
	stringstream averageTimeStr;
	if ((int)floor(averageTime) < 1) averageTimeStr << "00";
	else if ((int)floor(averageTime) < 10) averageTimeStr << "0";
	averageTimeStr << to_string((int)(floor(averageTime * 100)));

	//appending 0
	string numOfGames_str = Helper().getPaddedNumber(numOfGames, 5 - to_string(numOfGames).length());
	string correctAns_str = Helper().getPaddedNumber(correctAns, 7 - to_string(correctAns).length());
	string numOfWrongAnswers_str = Helper().getPaddedNumber(numOfWrongAnswers, 7 - to_string(numOfWrongAnswers).length());
	
	status.push_back(numOfGames_str);
	status.push_back(correctAns_str);
	status.push_back(numOfWrongAnswers_str);
	status.push_back(averageTimeStr.str());

	return status;
}


int DataBase::insertNewGame()
{
	stringstream command;
	command << "insert into t_games(status, start_time, end_time) values(0,date('now'),null);";
	int rc = sqlite3_exec(_db, command.str().c_str(), NULL, NULL, &zErrMsg);
	if (rc)
	{
		cout << "Can't insert the new game to database: " << sqlite3_errmsg(_db) << endl;
		return -1;
	}
	gameId++;
	return gameId;
}

bool DataBase::updateGameStatus(int id)
{
	stringstream command;
	command << "update t_games set end_time = date('now'), status = 1 where game_id = " << id << ";";
	int rc = sqlite3_exec(_db, command.str().c_str(), NULL, NULL, &zErrMsg);
	if (rc)
	{
		cout << "Can't update game status: " << sqlite3_errmsg(_db) << endl;
		return false;
	}
	return true;
}

bool DataBase::addAnswerToPlayer(int gameId, string username, int questionId, string answer, bool isCorrect, int answerTime)
{
	stringstream command;
	string isCorrect_str = "0";
	if (isCorrect) isCorrect_str = "1";
	command << "insert into t_players_answers(game_id, username, question_id,player_answer, is_correct,answer_time) values('" << to_string(gameId) << "', '" << username << "', '" << to_string(questionId) << "', '" << answer << "', '" << isCorrect_str << "', '" << to_string(answerTime) << "');";
	int rc = sqlite3_exec(_db, command.str().c_str(), NULL, NULL, &zErrMsg);
	if (rc)
	{
		cout << "Can't insert the answer to user in database: " << sqlite3_errmsg(_db) << endl;
		return false;
	}
	return true;
}

int DataBase::callbackCount(void* notUsed, int argc, char** argv, char** azCol)
{
	countUsers++;
	return 0;
}

int DataBase::callbackQuestions(void* notUsed, int argc, char** argv, char** azCol)
{
	Question *q = new Question(atoi(argv[0]), string(argv[1]), string(argv[2]), string(argv[3]), string(argv[4]), string(argv[5]));
	questions.push_back(q);
	return 0;
}

int DataBase::callbackBestScores(void* notUsed, int argc, char** argv, char** azCol)
{
	//argv[4] - is_correct, argv[1] - username
	if (atoi(argv[4]))//correct, add to score
	{
		std::map<string, int>::iterator it = usersAndScores.find(argv[1]);
		if (it == usersAndScores.end())//didn't find
			usersAndScores[argv[1]] = 1;
		else
			usersAndScores[argv[1]]++;
	}
	return 0;
}

int DataBase::callbackPersonalStatus(void* notUsed, int argc, char** argv, char** azCol)
{
	//argv[0] gameid, argv[1] username, argv[2] questionid, argv[3] answer, argv[4] is_correct,
	//argv[5] answertime
	if (atoi(argv[0]) > numOfGames) numOfGames = atoi(argv[0]);
	timeForAllQuestions += stof(argv[5]);
	questionsNum++;
	if (atoi(argv[4])) correctAns++;

	return 0;
}