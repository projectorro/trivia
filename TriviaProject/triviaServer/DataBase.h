#pragma once

#include "sqlite3.h"
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include "Question.h"
#include "Helper.h"
#include <fstream>
#include <map>
#include <math.h>

using namespace std;

class DataBase
{
public:
	DataBase();
	~DataBase();

	bool isUserExists(string);
	bool addNewUser(string, string, string);
	bool isUserAndPassMatch(string, string);
	vector<Question*> initQuestions(int);
	vector<string> getBestScores(); //TODO
	vector<string> getPersonalStatus(string); //TODO
	int insertNewGame();
	bool updateGameStatus(int);
	bool addAnswerToPlayer(int, string, int, string, bool, int);


private:
	int static callbackCount(void*, int, char**, char**);
	int static callbackQuestions(void*, int, char**, char**);
	int static callbackBestScores(void*, int, char**, char**);
	int static callbackPersonalStatus(void*, int, char**, char**);

	sqlite3* _db;
};
