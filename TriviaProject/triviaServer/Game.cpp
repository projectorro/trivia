#include "Game.h"

int Game::_currTurnAnswers = 0;

Game::Game(const vector<User*>& players, int questionsNo, DataBase& db) : _db(db)
{
	_id = _db.insertNewGame();
	if (_id == -1)
	{
		cout << "Error from game constructor, couln't add game." << endl;
		throw exception();
	}
	_questions = _db.initQuestions(questionsNo);
	_players = players;
	_questions_no = questionsNo;
	for (auto it = _players.begin(); it != _players.end(); ++it) //set current game to every user, update results
	{
		(*it)->setGame(this);
		_results[(*it)->getUserName()] = 0;
	}
}


Game::~Game()
{
}

void Game::sendFirstQuestion()
{
	initQuestionsFromDB();
	_currQuestionIndex = 0;
	sendQuestionToAllUsers();
}

void Game::handleFinishGame()
{
	stringstream m;
	_db.updateGameStatus(getId());
	m << "121" << to_string(_players.size());
	for each(User* user in _players)
	{
		if (user->getUserName().length() < 10) m << "0";
		m << to_string(user->getUserName().length()) << user->getUserName();
		if (_results[user->getUserName()] < 10) m << "0";
		m << to_string(_results[user->getUserName()]);
	}
	for each(User* user in _players)
	{
		try {
			user->send(m.str());
		}
		catch (exception &e) {}
	}
}


bool Game::handleNextTurn()
{
	if (_players.empty()) //no players
	{
		handleFinishGame();
		return false;
	}
	if (_currTurnAnswers == _players.size()) //if everybody answered
	{
		if (_currQuestionIndex + 1 == _questions_no) //out of questions
		{
			handleFinishGame();
			return false;
		}
		else {
			_currQuestionIndex++;
			sendQuestionToAllUsers();
			_currTurnAnswers = 0;
			return true;
		}
	}

	else { 
		_currQuestionIndex++;
		return true;
	}
}

bool Game::handleAnswerFromUSer(User *user, int answerNo, int time)
{
	bool was_right = false;
	int was_right_int = 0;
	_currTurnAnswers++;
	if (answerNo-1 == _questions[_currQuestionIndex]->getCorrectAnswerIndex()) //user was right, adds one to his score
	{
		was_right = true;
		was_right_int = 1;
		_results[user->getUserName()]++;
	}

	if (answerNo == 5)
		_db.addAnswerToPlayer(_id, user->getUserName(), _currQuestionIndex, "null", was_right, time);
	else _db.addAnswerToPlayer(_id, user->getUserName(), _currQuestionIndex, to_string(answerNo), was_right, time);
	stringstream s;
	s << "120" << was_right_int;
	user->send(s.str());
	return handleNextTurn();
}

bool Game::leaveGame(User *u)
{
	auto i = find(_players.begin(), _players.end(), u);
	if (i != _players.end()) //found user
	{
		_players.erase(i);
		if (_players.empty()) //no players
		{
			handleFinishGame();
			return false;
		}
		handleNextTurn();
	}
	return true;
}

int Game::getId()
{
	return _id;
}

bool Game::insertGameToDB()
{
	//TODO find out what is it
	return false;
}

void Game::initQuestionsFromDB()
{
	_questions = _db.initQuestions(_questions_no);
}

void Game::sendQuestionToAllUsers()
{
	_currTurnAnswers = 0;
	stringstream message;
	stringstream qLenString;
	int qLen = _questions[_currQuestionIndex]->getQuestion().length();
	if (qLen < 10)
		qLenString << "00" << to_string(qLen);
	else if (qLen < 100)
		qLenString << "0" << to_string(qLen);
	else qLenString << to_string(qLen);
	message << "118" << qLenString.str() << _questions[_currQuestionIndex]->getQuestion();
	for (int i = 0; i < 4; i++)
	{
		stringstream ansLenString;
		int ansLen = _questions[_currQuestionIndex]->getAnswers()[i].length();
		if (ansLen < 10)
			ansLenString << "00" << to_string(ansLen);
		else if (ansLen < 100)
			ansLenString << "0" << to_string(ansLen);
		else ansLenString << to_string(qLen);
		message << ansLenString.str() << _questions[_currQuestionIndex]->getAnswers()[i];
	}
	for (auto it = _players.begin(); it != _players.end(); ++it)
	{
		try {
			(*it)->send(message.str());
		}
		catch (exception &e) {}
	}
}
