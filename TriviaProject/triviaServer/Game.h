#pragma once

#include <vector>
#include "User.h"
#include "DataBase.h"	
#include <map>
#include "Question.h"
#include <string>
#include <iostream>
#include <mutex>

class User;

using namespace std;

class Game
{
public:
	static int _currTurnAnswers;

	Game(const vector<User*>&, int, DataBase&);
	~Game();

	void sendFirstQuestion();
	void handleFinishGame();
	bool handleNextTurn();
	bool handleAnswerFromUSer(User*, int, int);
	bool leaveGame(User*);
	int getId();

private:
	bool insertGameToDB();
	void initQuestionsFromDB();
	void sendQuestionToAllUsers();

	vector<Question*> _questions;
	vector<User*> _players;
	int _questions_no;
	int _currQuestionIndex;
	DataBase& _db;
	map<string, int> _results;
	//int _currTurnAnswers;
	int _id;
};

