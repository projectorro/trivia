#pragma once
#include <vector>
#include <string>
#include <WinSock2.h>
#include <iomanip>
#include <sstream>

#pragma comment (lib, "Ws2_32.lib")

class Helper
{
public:
	static int getMessageTypeCode(SOCKET sc);
	static char* getPartFromSocket(SOCKET sc, int bytesNum, int flags);
	static int getIntPartFromSocket(SOCKET sc, int bytesNum);
	static std::string getStringPartFromSocket(SOCKET sc, int bytesNum);
	static void sendData(SOCKET sc, std::string message);
	static std::string getPaddedNumber(int num, int digits);
private:
	static char* getPartFromSocket(SOCKET sc, int bytesNum);
};

