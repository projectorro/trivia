#include "Question.h"



Question::Question(int id, string question, string correctAnswer, string answer2, string answer3, string answer4)
{
	_id = id;
	_question = question;
	//srand(time(NULL));
	_correctAnswerIndex = rand() % 4;
	int i2 = rand() % 4, i3 = rand() % 4, i4 = rand() % 4;
	while (i2 == _correctAnswerIndex) i2 = rand() % 4;
	while (i3 == _correctAnswerIndex || i3 == i2) i3 = rand() % 4;
	while (i4 == _correctAnswerIndex || i4 == i3 || i4 == i2) i4 = rand() % 4;
	_answers[_correctAnswerIndex] = correctAnswer;
	_answers[i2] = answer2;
	_answers[i3] = answer3;
	_answers[i4] = answer4;
}

Question::~Question()
{
}

string Question::getQuestion()
{
	return _question;
}

string * Question::getAnswers()
{
	return _answers;
}

int Question::getCorrectAnswerIndex()
{
	return _correctAnswerIndex;
}

int Question::getId()
{
	return _id;
}
