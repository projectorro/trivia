#include "RecievedMessage.h"


RecievedMessage::RecievedMessage(SOCKET sock, int messageCode)
{
	_sock = sock;
	_messageCode = messageCode;
}

RecievedMessage::RecievedMessage(SOCKET sock, int messageCode, vector<string> values)
{
	_sock = sock;
	_messageCode = messageCode;
	_values = values;
}

RecievedMessage::~RecievedMessage()
{
}

SOCKET RecievedMessage::getSock()
{
	return _sock;
}

User * RecievedMessage::getuser()
{
	return this->_user;
}

void RecievedMessage::setUser(User *u)
{
	this->_user = new User(*u);
}

int RecievedMessage::getMessageCode()
{
	return this->_messageCode;
}

vector<string>& RecievedMessage::getValues()
{
	return this->_values;
}
