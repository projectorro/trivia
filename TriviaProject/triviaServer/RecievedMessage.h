#pragma once

#include "sqlite3.h"
#include "User.h"
#include <iostream>
#include <vector>
#include <thread>
#include <string>
#include <winsock2.h>

using namespace std;

class RecievedMessage
{
public:
	RecievedMessage(SOCKET, int);
	RecievedMessage(SOCKET, int, vector<string>);
	~RecievedMessage();

	SOCKET getSock();
	User* getuser();
	void setUser(User*);
	int getMessageCode();
	vector<string>& getValues();

private:
	SOCKET _sock;
	User* _user;
	int _messageCode;
	vector<string> _values;
};

