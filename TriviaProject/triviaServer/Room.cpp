#include "Room.h"


Room::Room(int id, User* admin, string name, int maxUsers, int questionsNo, int questionTime)
{
	this->_id = id;
	this->_admin = admin;
	this->_users.push_back(admin);
	this->_name = name;
	this->_maxUsers = maxUsers;
	this->_questionNo = questionsNo;
	this->_questionTime = questionTime;
}


Room::~Room()
{
}

bool Room::joinRoom(User *u)
{
	if (this->_users.size() == this->_maxUsers)
		return false; //no space
	this->_users.push_back(u);
	return true;
}

void Room::leaveRoom(User *u)
{
	auto i = find(_users.begin(), _users.end(), u);
	if (i != _users.end()) //found user
		_users.erase(i);
	//TODO check if works
}

int Room::closeRoom(User *u)
{
	/*
	//if succeed return its ID, if not return -1
	auto i = find(_users.begin(), _users.end(), u);
	if (i != _users.end()) //found user
	{
	if (_admin != (*i)) //TODO check if works
	return -1;
	}
	else return -1;//didn't find user
	*/
	if (_admin != u) //TODO check if works
		return -1;
	for (auto it = _users.begin(); it != _users.end(); ++it)
	{
		(*it)->clearRoom();
		(*it)->send("116");
	}
	return _id;
}

int Room::getQuestionTime()
{
	return this->_questionTime;
}

vector<User*> Room::getUsers()
{
	return _users;
}

string Room::getUsersListMessage()
{
	stringstream message;
	message << "108" << to_string(_users.size());
	for (auto it = _users.begin(); it != _users.end(); ++it)
	{
		if ((*it)->getUserName().length() < 10) message << "0";
		message << (*it)->getUserName().length() << (*it)->getUserName();
	}
	return message.str();
}

int Room::getQuestionsNo()
{
	return this->_questionNo;
}

int Room::getId()
{
	return this->_id;
}

string Room::getName()
{
	return this->_name;
}

string Room::getUsersAsString(vector<User*>, User *)
{
	return string();
}

void Room::sendMessage(string message)
{
	sendMessage(nullptr, message);
}

void Room::sendMessage(User *user, string message)
{
	for (auto it = _users.begin(); it != _users.end(); ++it)
		if ((*it) != user) Helper().sendData((*it)->getSocket(), message);
}
