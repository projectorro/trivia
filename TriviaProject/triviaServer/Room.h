#pragma once

#include <vector>
#include "User.h"
//#include "Helper.h"
#include <iostream>
#include <string>

using namespace std;

class User;

class Room
{
public:
	Room(int, User*, string, int, int, int);
	~Room();

	bool joinRoom(User*);
	void leaveRoom(User*);
	int closeRoom(User*);
	int getQuestionTime();
	vector<User*> getUsers();
	string getUsersListMessage();
	int getQuestionsNo();
	int getId();
	string getName();

private:
	string getUsersAsString(vector<User*>, User*);
	void sendMessage(string);
	void sendMessage(User*, string);

	vector<User*> _users;
	User* _admin;
	int _maxUsers;
	int _questionTime;
	int _questionNo;
	string _name;
	int _id;
};
