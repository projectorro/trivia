#include "TriviaServer.h"

int TriviaServer::_roomIdSequence = 0;
std::mutex m;
bool ready = false;
std::condition_variable cv;

int type = 0;

TriviaServer::TriviaServer()
{
	WSADATA wsaData;
	int iResult;

	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		return;
	}

	_serverSocket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (_serverSocket == INVALID_SOCKET)
	{
		throw std::exception(__FUNCTION__ " - socket");

	}
}


TriviaServer::~TriviaServer()
{
	try
	{
		::closesocket(_serverSocket);
	}
	catch (...) {}
}

void TriviaServer::server()
{
	thread t(&TriviaServer::handleRecievedMessages, this);
	t.detach();
	bindAndListen();
}

void TriviaServer::bindAndListen()
{
	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(PORT);
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = INADDR_ANY;
	if (::bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	if (::listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	cout << "Listening on port " << PORT << endl;

	while (true)
	{
		cout << "Waiting for client connection request" << endl;
		accept();
	}
}

void TriviaServer::accept()
{
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	cout << "Client accepted. Server and client can speak" << endl;

	thread t(&TriviaServer::clientHandler, this, client_socket);
	t.detach();
}

void TriviaServer::clientHandler(SOCKET clientSocket)
{
	while (true)
	{
		type = Helper().getMessageTypeCode(clientSocket);
		addRecievedMessage(buildRecieveMessage(clientSocket, type));
		std::lock_guard<std::mutex> lk(m);
		ready = true;
		cv.notify_one();
	}
}

void TriviaServer::safeDeleteUser(RecievedMessage *message)
{
	try
	{
		handleSignout(message);
		closesocket(message->getSock());
	}
	catch (exception &e)
	{
		cout << e.what() << endl;
	}
}

User * TriviaServer::handleSignin(RecievedMessage *message)
{
	if (users.count(message->getValues()[0]) != 1 || users[message->getValues()[0]] != message->getValues()[1])
	{
		Helper().sendData(message->getSock(), "1021");
		return nullptr;
	}
	if (getUserByName(message->getValues()[0]) != nullptr)
	{
		Helper().sendData(message->getSock(), "1022");
		return nullptr;
	}
	Helper().sendData(message->getSock(), "1020");
	return new User(message->getValues()[0], message->getSock());
}

bool TriviaServer::handleSignup(RecievedMessage *message)
{
	if (!isPasswordValid(message->getValues()[1]))
	{
		Helper().sendData(message->getSock(), "1041");
		return false;
	}
	if (!isUsernameValid(message->getValues()[0]))
	{
		Helper().sendData(message->getSock(), "1043");
		return nullptr;
	}
	if (users.count(message->getValues()[0]))
	{
		Helper().sendData(message->getSock(), "1042");
		return false;
	}
	Helper().sendData(message->getSock(), "1040");
	users[message->getValues()[0]] = message->getValues()[1];
	_db.addNewUser(message->getValues()[0], message->getValues()[1], message->getValues()[2]);
	return true;
}

void TriviaServer::handleSignout(RecievedMessage *message)
{
	if (_connectedUsers[message->getSock()] != nullptr)
	{
		_connectedUsers.erase(message->getSock());
	}
}

void TriviaServer::handleLeaveGame(RecievedMessage *message)
{
	if (_connectedUsers[message->getSock()]->leaveGame()) _connectedUsers[message->getSock()]->getGame()->~Game();
}

void TriviaServer::handleStartGame(RecievedMessage *message)
{
	try {
		Game *game = new Game(_connectedUsers[message->getSock()]->getRoom()->getUsers(), _connectedUsers[message->getSock()]->getRoom()->getQuestionsNo(), this->_db);
		_connectedUsers[message->getSock()]->setGame(game);
		_roomList.erase(_connectedUsers[message->getSock()]->getRoom()->getId());
		_roomIdSequence--;
		_connectedUsers[message->getSock()]->getGame()->sendFirstQuestion();
	}
	catch (exception &e) {
		cout << e.what() << endl;
		_connectedUsers[message->getSock()]->send("1180");
	}
}

void TriviaServer::handlePlayerAnswer(RecievedMessage *msg)
{
	if (_connectedUsers[msg->getSock()]->getGame() != nullptr)
		if (_connectedUsers[msg->getSock()]->getGame()->handleAnswerFromUSer(_connectedUsers[msg->getSock()], atoi(msg->getValues()[0].c_str()), atoi(msg->getValues()[1].c_str())) == false)
			_connectedUsers[msg->getSock()]->getGame()->~Game(); //empties game.
}

bool TriviaServer::handleCreateRoom(RecievedMessage *msg)
{
	if (_connectedUsers[msg->getSock()] == nullptr) return false;
	if (_connectedUsers[msg->getSock()]->createRoom(this->_roomIdSequence, msg->getValues()[0], stoi(msg->getValues()[1]), stoi(msg->getValues()[2]), stoi(msg->getValues()[3])) != false)
		this->_roomList[_roomIdSequence] = _connectedUsers[msg->getSock()]->getRoom();
	this->_roomIdSequence++;
	return true;
}

bool TriviaServer::handleCloseRoom(RecievedMessage *msg)
{
	if (_connectedUsers[msg->getSock()]->getRoom() == nullptr) return false;
	int num = _connectedUsers[msg->getSock()]->closeRoom();
	if (num != -1)
	{
		this->_roomList.erase(num);
		return true;
	}
	return false;
}

bool TriviaServer::handleJoinRoom(RecievedMessage *message)
{
	if (_connectedUsers[message->getSock()] == nullptr)
		return false;
	if (getRoomById(atoi(message->getValues()[0].c_str())) == nullptr)
	{
		_connectedUsers[message->getSock()]->send("1102");
		return false;
	}
	_connectedUsers[message->getSock()]->joinRoom(getRoomById(atoi(message->getValues()[0].c_str())));
	string users = "1100";
	if (_connectedUsers[message->getSock()]->getRoom()->getQuestionsNo() < 10) users += "0";
	users += to_string(_connectedUsers[message->getSock()]->getRoom()->getQuestionsNo());
	if (_connectedUsers[message->getSock()]->getRoom()->getQuestionTime() < 10) users += "0";
	users += to_string(_connectedUsers[message->getSock()]->getRoom()->getQuestionTime());
	_connectedUsers[message->getSock()]->send(users);
	stringstream usersCommand;
	usersCommand << "108" << _connectedUsers[message->getSock()]->getRoom()->getUsers().size();
	for each(User* user in _connectedUsers[message->getSock()]->getRoom()->getUsers())
	{
		if (user->getUserName().length() < 10) usersCommand << "0";
		usersCommand << to_string(user->getUserName().length()) + user->getUserName();
	}
	for each(User* user in _connectedUsers[message->getSock()]->getRoom()->getUsers())
		user->send(usersCommand.str());
	return true;
}

bool TriviaServer::handleLeaveRoom(RecievedMessage *message)
{
	if (_connectedUsers[message->getSock()] == nullptr || _connectedUsers[message->getSock()]->getRoom() == nullptr) return false;
	_connectedUsers[message->getSock()]->leaveRoom();
	return true;
}

void TriviaServer::handleGetUsersInRoom(RecievedMessage *message)
{
	Room *r = getRoomById(atoi((message->getValues()[0]).c_str()));
	if (r != nullptr) _connectedUsers[message->getSock()]->send(r->getUsersListMessage());
	else _connectedUsers[message->getSock()]->send("1080");
}

void TriviaServer::handleGetRooms(RecievedMessage *message)
{
	string size = "";
	if (_roomIdSequence < 1000)
	{
		size += "0";
		if (_roomIdSequence < 100)
		{
			size += "0";
			if (_roomIdSequence < 10)
			{
				size += "0";
			}
		}
	}
	size += to_string(_roomIdSequence);
	string m = "106" + size;
	for (int i = 0; i < _roomIdSequence && i < 10; i++) {
		m += "000" + to_string(i);
		if (_roomList[i]->getName().length() < 10) m += "0";
		m += to_string(_roomList[i]->getName().length());
		m += _roomList[i]->getName();
	}
	for (int i = 10; i < _roomIdSequence && i < 100; i++) {
		m += "00" + i;
		if (_roomList[i]->getName().length() < 10) m += "0";
		m += to_string(_roomList[i]->getName().length());
		m += _roomList[i]->getName();
	}
	for (int i = 100; i < _roomIdSequence && i < 1000; i++) {
		m += "0" + i;
		if (_roomList[i]->getName().length() < 10) m += "0";
		m += to_string(_roomList[i]->getName().length());
		m += _roomList[i]->getName();
	}
	for (int i = 1000; i < _roomIdSequence; i++) {
		m += i;
		if (_roomList[i]->getName().length() < 10) m += "0";
		m += to_string(_roomList[i]->getName().length());
		m += _roomList[i]->getName();
	}
	Helper().sendData(message->getSock(), m);
}

void TriviaServer::handleGetBestScores(RecievedMessage *message)
{
	//len = 2 bytes, score = 6 bytes
	//bestscores: "[uname1_len][uname1][score][uname2_len][uname2][score][uname3_len][uname3][score]"
	stringstream s;
	vector<string> bestScores = _db.getBestScores();
	s << "124";
	for (int i = 0; i < 9; i += 3) //0 1 2 3 4 5 6 7 8
		s << bestScores[i] << bestScores[i + 1] << bestScores[i + 2];
	Helper().sendData(message->getSock(), s.str());
}

void TriviaServer::handleGetPersonalStatus(RecievedMessage *msg)
{
	//status contains: number of games (4 bytes), number of right answers (6 bytes),
	//number of wrong answers (6 bytes) and avarage answer time (4 bytes for example: 0354 is 3.54)
	stringstream s;
	vector<string> status = _db.getPersonalStatus(getUserBySocket(msg->getSock())->getUserName());
	s << "126" << status[0] << status[1] << status[2] << status[3];
	Helper().sendData(msg->getSock(), s.str());
}

void TriviaServer::handleRecievedMessages()
{
	while (true)
	{
		std::unique_lock<std::mutex> lk(m);
		cv.wait(lk, [] {return ready; });
		ready = false;
		_mtxRecievedMessages.lock();
		bool empty = _queRcvMessages.empty();
		_mtxRecievedMessages.unlock();
		if (!empty)
		{
			_mtxRecievedMessages.lock();
			RecievedMessage *message = _queRcvMessages.front();
			_queRcvMessages.pop();
			_mtxRecievedMessages.unlock();
			switch (message->getMessageCode())
			{
			case 200:
				_connectedUsers[message->getSock()] = handleSignin(message);
				break;
			case 201:
				handleSignout(message);
				break;
			case 203:
				handleSignup(message);
				break;
			case 205:
				handleGetRooms(message);
				break;
			case 207:
				handleGetUsersInRoom(message);
				break;
			case 209:
				handleJoinRoom(message);
				break;
			case 211:
				handleLeaveRoom(message);
				break;
			case 213:
				if (handleCreateRoom(message)) _connectedUsers[message->getSock()]->send("1140");
				else  _connectedUsers[message->getSock()]->send("1141");
				break;
			case 215:
				handleCloseRoom(message);
				break;
			case 217:
				handleStartGame(message);
				break;
			case 219:
				handlePlayerAnswer(message);
				break;
			case 222:
				handleLeaveGame(message);
				break;
			case 223:
				handleGetBestScores(message);
				break;
			case 225:
				handleGetPersonalStatus(message);
				break;
			case 299:
				safeDeleteUser(message);
				break;
			}
		}
	}
}

void TriviaServer::addRecievedMessage(RecievedMessage *message)
{
	_mtxRecievedMessages.lock();
	_queRcvMessages.push(message);
	_mtxRecievedMessages.unlock();
}

RecievedMessage * TriviaServer::buildRecieveMessage(SOCKET clientSocket, int type)
{
	vector<int> noValues = { 201, 205, 211, 215, 217, 222, 223, 225, 299 };
	if (find(noValues.begin(), noValues.end(), type) != noValues.end())
		return new RecievedMessage(clientSocket, type);
	else
	{
		vector<string> values;
		switch (type)
		{
		case 203:
			values.push_back(Helper().getStringPartFromSocket(clientSocket, Helper().getIntPartFromSocket(clientSocket, 2)));
		case 200:
			values.push_back(Helper().getStringPartFromSocket(clientSocket, Helper().getIntPartFromSocket(clientSocket, 2)));
			values.push_back(Helper().getStringPartFromSocket(clientSocket, Helper().getIntPartFromSocket(clientSocket, 2)));
			break;
		case 207:case 209:
			values.push_back(Helper().getStringPartFromSocket(clientSocket, 4));
			break;
		case 213:
			values.push_back(Helper().getStringPartFromSocket(clientSocket, Helper().getIntPartFromSocket(clientSocket, 2)));
			values.push_back(Helper().getStringPartFromSocket(clientSocket, 1));
			values.push_back(Helper().getStringPartFromSocket(clientSocket, 2));
			values.push_back(Helper().getStringPartFromSocket(clientSocket, 2));
			break;
		case 219:
			values.push_back(Helper().getStringPartFromSocket(clientSocket, 1));
			values.push_back(Helper().getStringPartFromSocket(clientSocket, 2));
			break;
		}
		return new RecievedMessage(clientSocket, type, values);
	}
}

User * TriviaServer::getUserByName(string name)
{
	for (auto it = _connectedUsers.begin(); it != _connectedUsers.end(); ++it)
	{
		if ((*it).second->getUserName() == name)
			return (*it).second;
	}
	return nullptr;
}

User * TriviaServer::getUserBySocket(SOCKET socket)
{
	return _connectedUsers[socket];
}

Room * TriviaServer::getRoomById(int id)
{
	return _roomList[id];
}
