#include "User.h"



User::User(string uname, SOCKET soc)
{
	_username = uname;
	_socket = soc;
}


User::~User()
{
	delete _currRoom;
	delete _currGame;
}

void User::send(string str)
{
	Helper().sendData(_socket, str);
}

string User::getUserName()
{
	return _username;
}

SOCKET User::getSocket()
{
	return _socket;
}

Room * User::getRoom()
{
	return _currRoom;
}

Game * User::getGame()
{
	return _currGame;
}

void User::setGame(Game *g)
{
	Game* game = new Game(*g);
	_currGame = game;
}

void User::clearRoom()
{
	_currRoom = nullptr;
}

bool User::createRoom(int id, string roomName, int maxUsers, int questionsNo, int questionTime)
{
	if (_currRoom != nullptr) return false;
	_currRoom = new Room(id, this, roomName, maxUsers, questionsNo, questionTime);

	return true;
}

bool User::joinRoom(Room *newRoom)
{
	if (_currRoom == nullptr && newRoom->joinRoom(this))
	{
		_currRoom = newRoom;
		return true;
	}
	return false;
}

void User::leaveRoom()
{
	if (_currRoom != nullptr)
	{
		_currRoom->leaveRoom(this);
		_currRoom = nullptr;
	}
}

int User::closeRoom()
{
	if (_currRoom == nullptr || _currRoom->closeRoom(this) == -1) return -1;
	_currRoom = nullptr;
	return 0;
}

bool User::leaveGame()
{
	if (_currGame == nullptr || _currGame->leaveGame(this))
	{
		_currGame = nullptr;
		return false;
	}
	return true;
}
