#pragma once

#include "Helper.h"
#include <WinSock2.h>
#include <iomanip>
#include <sstream>
#include <string>
#include "Room.h"
#include "Game.h"
#include <map>

using namespace std;

class Game;
class Room;

class User
{
public:
	User(string uname, SOCKET soc);
	~User();

	void send(string);
	string getUserName();
	SOCKET getSocket();
	Room* getRoom();
	Game* getGame();
	void setGame(Game*);
	void clearRoom();
	bool createRoom(int, string, int, int, int);
	bool joinRoom(Room*);
	void leaveRoom();
	int closeRoom();
	bool leaveGame();

private:
	string _username;
	Room* _currRoom;
	Game* _currGame;
	SOCKET _socket;
};