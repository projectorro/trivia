#pragma once

#include <iostream>

using namespace std;

static bool isPasswordValid(string password)
{
	if (password.length() < 4) return false;
	for each (char c in password) if (c == ' ') return false;
	for each (char c in password) if (isupper(c)) for each (char c2 in password) if (islower(c2)) for each (char c3 in password) if (isdigit(c3))return true;
	return false;
}

static bool isUsernameValid(string username)
{
	if (!username.empty() && isalpha(username[0]))
	{
		for each (char c in username) if (c == ' ') return false;
		return true;
	}
	return false;
}
